from flask                   import Flask,request,jsonify
from core.db_access.db_mysql import Acesso
from core.telegram.monitor   import Monitor

app = Flask(__name__)

from rotas import *

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)