from flask                   import Flask,request,jsonify
from core.db_access.db_mysql import Acesso
from core.telegram.monitor   import Monitor
from __main__ import app

@app.route('/', methods = ['GET'])
def home():
    return '<h1>API-teste está on-line!</h1>',200


@app.route('/dados_db', methods = ['GET'])
def teste():
    db = Acesso()  
    return jsonify({'Banco de dados': format(db.conexao.database) ,'Host': format(db.conexao.host), 'Senha' : format(db.conexao.password), 'Usuario' : format(db.conexao.user), 'Porta' : format(db.conexao.port)})


@app.route('/clientes', methods = ['GET'])
def clientes():
    db = Acesso()
    resultado = db.executar(1,'select * from clientes limit 10')
    if resultado == False:
        resultado = 'Dados não encontrados.'

    return jsonify(resultado)

