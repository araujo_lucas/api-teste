import telepot
from core.telegram.settings import MEU_ID, TELEGRAM_TOKEN

class Monitor:

    def __init__(self,id_telegram=MEU_ID,token=TELEGRAM_TOKEN):
        
        self.id_telegram = id_telegram
        self.token       = token
        

    def send(self,mensagem):
        
        bot = telepot.Bot(self.token)
        bot.sendMessage(self.id_telegram,str(mensagem))
        