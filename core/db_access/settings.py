import os
from dotenv import load_dotenv

load_dotenv()


HOST     = os.getenv("HOST")
PASSWORD = os.getenv("PASSWORD")
USER     = os.getenv("USER")
DATABASE = os.getenv("DATABASE")
PORT     = os.getenv("PORT")