from mysql.connector       import connect
from core.telegram.monitor import Monitor

from core.db_access.settings import HOST,DATABASE,USER,PASSWORD,PORT


class Conexao:

    def __init__(self,host,database,user,password,port):

        self.host     = host
        self.database = database
        self.user     = user
        self.password = password
        self.port     = port


class Acesso:
    
    def __init__(self):
        
        self.conexao = Conexao(HOST,DATABASE,USER,PASSWORD,PORT)

    
    def conectar(self, host=None, database=None, user=None, password=None, port=None):

        self.conexao = Conexao(
            (host,     self.conexao.host)     [host is None],
            (database, self.conexao.database) [database is None],
            (user,     self.conexao.user)     [user is None],
            (password, self.conexao.password) [password is None],
            (port,     self.conexao.port)     [port is None]
        )

    
    def executar(self,operacao,query):
        ''' Método para executar comandos sql, 
        operação 1 para select e 2 para inserts, updates e alterações.
        '''

        try:

            connection = connect(
                host     = self.conexao.host,
                database = self.conexao.database,
                user     = self.conexao.user,
                password = self.conexao.password,
                port     = self.conexao.port
            )

            cursor = connection.cursor()

        except Exception as erro:

            monitor = Monitor()
            monitor.send('Erro de conexao ao banco de dados: ' + format(erro))
            
            return False

        else:

            try:
                if operacao == 1:

                    cursor.execute(query)
                    resultado = cursor.fetchall()
                    return resultado

                elif operacao == 2:

                    cursor.execute(query)
                    connection.commit()
                    return True
                
                else:

                    print('operação invalida')
                    return False

            
            except Exception as erro:
                monitor = Monitor()
                monitor.send('Erro ao executar a query: ' + format(erro))
                
                return False
            finally:

                cursor.close()
                connection.close()

